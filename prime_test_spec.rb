require_relative "primenumber"
require "test/unit"

class PrimeTest < Test::Unit::TestCase

  def test_primes
    assert_equal(3, primeNumber(2))
    assert_equal(7, primeNumber(4))
    assert_equal(13, primeNumber(6))
    assert_equal(104743, primeNumber(10001))
  end

end