def primeNumber(input)
  # ini variables
  numberOfPrimes = 0
  number = 1
  maxLimit = 999999 #setLimit
  sieve = Array.new

  # loop from 2 through to limit
  (2..maxLimit).each { |i|
    # if number is already in array (and is true), skip it and go to the next number
    if sieve[i]
      next
    end

    # found a prime
    numberOfPrimes += 1

    # got a result, save the number and exit the loop
    if numberOfPrimes == input
      number = i
      break
    end

    # find the multiplicity of the number and mark it as true
    ((i+i)..maxLimit).step(i) do |j|
      sieve[j] = true
    end
  }

  # show the number of prime
  number
end

p primeNumber(10001)